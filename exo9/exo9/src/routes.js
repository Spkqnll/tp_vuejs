
import About from './components/About.vue';
import Formulaire from './components/Formulaire.vue';
import Home from './components/Home.vue';

const routes = [
    { path: '/about', component: About },
    { path: '/formulaire', component: Formulaire },
    { path: '/', component: Home },
    
];

export default routes;